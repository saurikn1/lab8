package com.saurik.matrix

import jdk.nashorn.internal.runtime.regexp.joni.Config.log

class Multiply private constructor(private val a: Array<IntArray>, private val b: Array<IntArray>, private val result: Array<IntArray>, private val startRow: Int, private val endRow: Int) : Thread() {
    private val n: Int

    init {
        this.n = b.size
    }

    override fun run() {
        for (row in startRow..endRow) {
            for (col in 0..result[row].size - 1) {
                result[row][col] = getValue(row, col)
            }
        }
    }

    private fun getValue(row: Int, col: Int): Int {
        var c = 0
        for (i in 0..n - 1) {
            c += a[row][i] * b[i][col]
        }
        return c
    }

    companion object {

        private fun multiply(first: Array<IntArray>?, second: Array<IntArray>?): Array<IntArray> {
            if (first == null || first.size == 0 || first[0] == null || first[0].size == 0) {
                throw IllegalArgumentException("first")
            }
            if (second == null || second.size == 0 || second[0] == null || second[0].size == 0) {
                throw IllegalArgumentException("second")
            }
            if (first[0].size != second.size) {
                throw IllegalArgumentException("wrong matrix")
            }
            val m = first.size
            val q = second[0].size
            val result = Array(m) { IntArray(q) }
            val threadValue = 4
            val c = m / threadValue
            val add = m % threadValue
            val threads = arrayOfNulls<Thread>(threadValue)
            var start = 0
            for (i in 0..threadValue - 1) {
                val cnt = if (i == 0) c + add else c
                threads[i] = Multiply(first, second, result, start, start + cnt - 1)
                start += cnt
                threads[i]?.start()
            }
            return result
        }

        @JvmStatic
        fun main(args: Array<String>) {
            val a = arrayOf(intArrayOf(3, 8, 3, 2), intArrayOf(1, 6, 2, 8), intArrayOf(9, 1, 10, 3), intArrayOf(2, 1, 22, 14), intArrayOf(22, 13, 8, 2))
            val b = arrayOf(intArrayOf(2, 1, 2, 1, 2), intArrayOf(1, 2, 1, 2, 1), intArrayOf(2, 1, 2, 1, 2), intArrayOf(1, 2, 1, 2, 1))
            val c = multiply(a, b)

            for (ints in c) {
                for (anInt in ints) {
                    log.print(anInt.toString() + " ")
                }
                log.print("\n")
            }
        }
    }
}